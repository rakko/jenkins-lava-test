/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 expandtab: */
pipeline {
  agent {
    docker {
      image 'rakko/lqa'
      args "--entrypoint=''"
    }
  }

  stages {

    stage("List directory") {
      steps {
        sh 'ls -l'
      }
    }

    stage("Submit LAVA job and wait for results") {
      environment {
        LQA_CONFIG = 'lqa.yaml'
        LOG = 'gst-validate.log'
      }
      steps {
        writeFile(file: "${LQA_CONFIG}", text:
"""
user: '${params.LAVA_USER}'

auth-token: '${params.LAVA_AUTH_TOKEN}'

server: '${params.LAVA_SERVER}'
"""
        )
        script {
          hook = registerWebhook()

          // Submit
          sh 'echo Submitting LAVA job as $(lqa -c ${LQA_CONFIG} whoami)'
          sh "lqa -c ${LQA_CONFIG} --log-file ${LOG} submit -v -t callback_url:${hook.getURL()} gst-validate.yaml"

          jobid = sh(returnStdout: true, script: "set +x ; tail -n1 ${LOG} | awk '{print \$NF}'").trim()

          // Wait
          sh "echo Submitted job at http://lava.collabora.co.uk/scheduler/job/${jobid}..."
          sh "echo Waiting for job ${jobid} to finish..."
          data = waitForWebhook hook

          // Store Results
          sh "mkdir -p results"
          writeFile(file: "results/gst-validate-testsuites.xml", text: "${data}")

          // Parse Results
          junit "**/results/*.xml"

          // Summary of test results
          sh "lqa -c ${LQA_CONFIG} results ${jobid}"
        }
      }
    }
  }
}
